import { React, Fragment, useEffect, useState } from 'react';
import { Form, Button, Container, Row, Col, Dropdown} from 'react-bootstrap';
import { URLS } from '../api/Apis';
import axios from 'axios';
import  Result  from '../components/Result';

const Home = () => {

    const headers = {
        'Content-Type': 'application/json',
    }
    const [categories, setCategories] =  useState([]);
    const [newsWeather, setNewsWeather] =  useState({
        news: {
            articles:[{
                source: {
                    name:"",
                },
                author:"",
                title:"",
                description:"",
                url:"",
                publishedAt:"",
            }]
        },
        weathers:{
            weather:[{
                main:"",
                description:""
            }],
            main:{
                temp:0,
            }
        }
    });
    const [languages, setLanguages] =  useState([]);
    const [countries, setCountries] =  useState([]);
    const [filters, setFilter] = useState({
        q:"",
        category: undefined,
        language:undefined,
        lang:undefined,
        country:undefined,
        pageSize:undefined,
        page:undefined
    })

    
    const obtenerDatos = async () => {
        await axios.get(URLS["ApiLocalhost"]+"NewsWeather").then((res) => {
            const result = res.data;
            setCategories(result.category)
            setLanguages(result.language)
            setCountries(result.country)
            setFilter({
                q:"",
                category: undefined,
                language:undefined,
                lang:undefined,
                country:undefined,
                pageSize:undefined,
                page:undefined
            });
        }).catch(err=> console.log(err.message))

    }

    const enviarPeticion = async () =>{
        console.log(filters)
         await axios.post(URLS["ApiLocalhost"]+"NewsWeather/GetNewsAndWeather", JSON.stringify(filters), {
            headers: headers
          }).then( (res ) => {
            console.log(res.data);
            setNewsWeather(res.data);
        }).catch(err=> console.log("Error en catch perro:"+err))
        await obtenerDatos()
    }

    useEffect(() =>{
        obtenerDatos()
    },[])


    const filterChanged = (e, type) =>{
        let filterObj = Object.assign({}, filters);
        if(type===1)
            filterObj.q = e;
        if(type===2)
            filterObj.category = e;
        if(type===3)
            filterObj.language = e;
        if(type===4)
            filterObj.country = e;
        return filterObj;
    }
    const back  =async (e) => {
        e.preventDefault();
        await enviarPeticion(filters)
        
    }
    
    return ( 
        <Fragment>
            <div className="container">
                <div className="abs-center">
                <h1>Consultar Clima y Noticias</h1>
                
                <Form className="mt-5">
                    <Form.Group className="mb-3" controlId="form.Ciudad">
                        <Form.Label>Coloca aquí tu ciudad:</Form.Label>
                        <Form.Control value={filters.q} type="text" placeholder="Indicanos la Ciudad a Consultar" 
                        onChange={(e)=> setFilter(filterChanged(e.target.value, 1))}/>
                    </Form.Group>
                    <Button variant="success" type="submit" onClick={back}>
                        Enviar
                    </Button>
                <h4 className="mt-5">Filtros solo aplica para Noticias:</h4>
                <div onClick={e => e.preventDefault()} className="d-flex" >
                    <Container fluid="md">
                        <Row>
                        <Col sm>
                            
                            <Dropdown onSelect= {(e)=> setFilter(filterChanged(e, 2))} >
                                <Dropdown.Header>Categorías:</Dropdown.Header>
                                <Dropdown.Toggle id="dropdown-basic">
                                    {filters.category?filters.category: "Seleccionar Categoría"}
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    {
                                        categories.map(item => (
                                            <Dropdown.Item key={item} eventKey={item} as="button"  >{item}</Dropdown.Item>
                                        ))
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col sm>
                            <Dropdown onSelect={(e)=> setFilter(filterChanged(e, 3))}  >
                            <Dropdown.Header>lenguas ("es" por defecto):</Dropdown.Header>
                                <Dropdown.Toggle id="dropdown-basic">
                                    {filters.language?filters.language: "Seleccionar Idioma"}
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                
                                    {
                                        languages.map(item => (
                                            <Dropdown.Item key={item} eventKey={item} as="button"  >{item}</Dropdown.Item>
                                        ))
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col sm>
                            <Dropdown onSelect={(e)=> setFilter(filterChanged(e, 4))} >
                            <Dropdown.Header>Paises:</Dropdown.Header>
                                <Dropdown.Toggle id="dropdown-basic">
                                    {filters.country?filters.country: "Seleccionar Pais"}
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    {
                                        countries.map(item => (
                                            <Dropdown.Item key={item} eventKey={item} as="button"  >{item}</Dropdown.Item>
                                        ))
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col><Button className="mt-5" variant="warning" onClick={obtenerDatos}> LimpiarFiltros </Button></Col>
                        </Row>
                    </Container>
                </div>
                </Form>
                </div>
            </div>
            <div className="container">
                <Result value={newsWeather}/>
            </div>
        </Fragment>
    );
}
 
export default Home;
