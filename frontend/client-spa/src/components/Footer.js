import {React, Fragment} from 'react';
import './css/footer.css'

const Footer = () => {
    return ( 
        <Fragment>
            <div className="footer footer-stick">
                <div className="container">
                    <br></br>
                    <span className="text-muted">Realizado por Johan Emmanuel Valera Lopez para Malakai Group.</span>
                </div>
            </div>
        </Fragment>
    );
}
 
export default Footer;