import {React, Fragment, useEffect, useState} from 'react';
import { Table } from 'react-bootstrap';
import { URLS } from '../api/Apis';
import axios from 'axios';

const History = () => {

    const [history, setHistory] = useState([])
    const obtenerDatos = async () => {
        await axios.get(URLS["ApiLocalhost"]+"NewsWeather/GetHistory").then((res) => {
            const result = res.data;
            console.log(res.data)
            setHistory(result)
        }).catch(err=> console.log(err.message))

    }

    useEffect(() =>{
        obtenerDatos()
    },[])

    return ( 
        <Fragment>
            <div className="container">
                <h1>History</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>City</th>
                        <th>News founded</th>
                        <th>Weather Results</th>
                        <th>News Filtered</th>
                        <th>Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            history.map((hist, index)=>(
                                <tr>
                                <td>{index}</td>
                                <td>{hist.city}</td>
                                <td>{hist.newsFounded}</td>
                                <td>{hist.weatherResult}</td>
                                <td>{hist.newsFiltered? "Si": "No"}</td>
                                <td>{hist.dateCreated}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
        </Fragment>
     );
}
 
export default History;