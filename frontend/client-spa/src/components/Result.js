import {React} from 'react';
import '../components/css/result.css'

const Result = (newsWeather) =>{
    return(
        <div className="container cfooter text-center">
            <h3>News:</h3>
            {
                newsWeather.value.news.articles.map(article => (
                    <div>
                        <h5>{article.title}</h5>
                        <p>Descripcion: {article.description}</p>
                        <img src={article.urlToImage} alt=""/>
                        <p>Author: {article.author}</p>
                        <p>PublishedAt: {article.publishedAt}</p>
                        <p>Url: {article.url}</p>
                    </div>
                ))
            }
            <h3>Weather:</h3>
            {
                newsWeather.value.weathers.weather.map(weather => (
                    <div>
                        <p>{weather.main}</p>
                        <p>{weather.description}</p>
                    </div>
                ))
            }
            <strong>{newsWeather.value.weathers.main.temp-273.15+'°C'} </strong>
        </div>
    )
} 

export default Result;