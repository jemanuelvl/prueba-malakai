import { React, Fragment } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';

const Header = () => {
    return ( 
        <Fragment>
            <Navbar bg="dark" expand="lg" variant="dark">
            <Container>
                <Navbar.Brand href="home">Home</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link href="home">Consultar Noticias y cLima</Nav.Link>
                    <Nav.Link href="history">Historial de Peticiones</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>
        </Fragment>
     );
}
 
export default Header;
