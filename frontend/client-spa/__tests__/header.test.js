
import React from 'react'
import { render } from '@testing-library/react'
import { Header } from '../src/components/Header'

describe('Header', ()=>{
    it('must display menus titles', ()=>{
        const { container, getByText} = render(<Header />)
        expect(getByText(/Consultar Noticias y cLima/i)).toBeInTheDocument()
        expect(container.firstChild).toMatchInlineSnapshot(`<Nav.Link href="home">Consultar Noticias y cLima</Nav.Link>`)
    } )
})