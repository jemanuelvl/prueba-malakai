﻿using Common.Exceptions;
using Models.Models;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repository.SqlServer
{
    public class NewsRepository : Repository, INewsRepository
    {
        public NewsRepository(SqlConnection context, SqlTransaction transaction)
        {
            this._context = context;
            this._transaction = transaction;
        }
        public async Task<IEnumerable<ParameterInformationNews>> GetAllParameters()
        {
            try
            {
                var command = "dbo.GetAllCategoriesNewsApi";
                return await this.GetDataFromStoreProcedure<ParameterInformationNews>(command);
            }
            catch (Exception ex)
            {
                throw new GlobalExceptionError(Common.Errors.ErrorMessages.ERROR_ON_EXCECUTE_STORE_PROCEDURE, ex);
            }
        }
    }
}
