﻿using Common.Exceptions;
using Models.Models;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Repository.SqlServer
{
    public class HistoryRepository : Repository, IHistoryRepository
    {
        public HistoryRepository(SqlConnection context, SqlTransaction transaction)
        {
            this._context = context;
            this._transaction = transaction;
        }
        /// <summary>
        /// Get History of requests
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<History>> GetHistoryRequest()
        {
            try
            {
                var command = "dbo.GetAllHistoriesRequests";
                return await this.GetDataFromStoreProcedure<History>(command);
            }catch(Exception ex)
            {
                throw new GlobalExceptionError(Common.Errors.ErrorMessages.ERROR_ON_EXCECUTE_STORE_PROCEDURE, ex);
            }

        }

        public async Task InsertHistoryRequest(History history)
        {
            try
            {
                var command = "dbo.InsertHistoryRequest";
                await this.ExecuteSP(command, history);
            }
            catch (Exception ex)
            {
                throw new GlobalExceptionError(Common.Errors.ErrorMessages.ERROR_ON_EXCECUTE_STORE_PROCEDURE, ex);
            }
        }
    }
}
