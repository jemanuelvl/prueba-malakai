﻿using AutoMapper;
using Models.DTO;

namespace Models.Mapping
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<QueryParamNewsWeather, QueryParamWeather>();
            CreateMap<QueryParamNewsWeather, QueryParamNews>();
        }
    }
}
