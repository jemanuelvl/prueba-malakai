﻿using Models.Models;

namespace Models.DTO
{
    public class NewsWatherResponse
    {
        public NewsResponse News { get; set; }
        public WeatherResponse Weathers { get; set; }
    }
}
