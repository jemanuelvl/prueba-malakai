﻿namespace Models.DTO
{
    public class QueryParamNewsWeather
    {
        public string q { get; set; }
        public string? category { get; set; }
        public string? language { get; set; } = "es";
        public string? lang { get; set; } = "es";
        public string? country { get; set; }
        public int? pageSize { get; set; } = 10;
        public int? page { get; set; } = 1;
    }
}
