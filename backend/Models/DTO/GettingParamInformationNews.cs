﻿using System.Collections.Generic;

namespace Models.DTO
{
    public class GettingParamInformationNews
    {
        public List<string> category { get; set; }
        public List<string> language { get; set; }
        public List<string> country { get; set; }
    }
}
