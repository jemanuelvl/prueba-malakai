﻿namespace Models.DTO
{
    public class QueryParamWeather
    {
        public string? q { get; set; } = null;
        public string? lang { get; set; } = "es";
    }
}
