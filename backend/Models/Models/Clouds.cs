﻿using Newtonsoft.Json;

namespace Models.Models
{
    public class Clouds
    {
        [JsonProperty("all")]
        public long All { get; set; }
    }
}
