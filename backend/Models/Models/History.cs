﻿using Common.Attributes;
using System;

namespace Models.Models
{
    public class History
    {
        [Write(false)]
        public int IdHistoryRequest { get; set; }
        public string City { get; set; }
        public string NewsFounded { get; set; }
        public string WeatherResult { get; set; }
        public bool NewsFiltered { get; set; }
        [Write(false)]
        public DateTime DateCreated { get; set; }
    }
}
