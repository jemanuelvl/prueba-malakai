﻿using System;
using System.Collections.Generic;

namespace Models.Models
{
    /// <summary>
    /// Response of News
    /// </summary>
    public class NewsResponse
    {
        public string status { get; set; }
        public int totalResults { get; set; }
        public IEnumerable<Article> articles { get; set; }

        public override bool Equals(Object obj)
        {
            if (obj is NewsResponse)
            {
                var that = obj as NewsResponse;
                return this.status == that.status && this.totalResults == that.totalResults && this.articles == that.articles;
            }

            return false;
        }
    }
}
