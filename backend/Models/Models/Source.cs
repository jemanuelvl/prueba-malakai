﻿using System;

namespace Models.Models
{
    /// <summary>
    /// Source
    /// </summary>
    public class Source
    {
        public string id { get; set; }
        public string name { get; set; }

        public override bool Equals(Object obj)
        {
            if (obj is Source)
            {
                var that = obj as Source;
                return this.id == that.id &&
                    this.name == that.name;
            }

            return false;
        }
    }
}
