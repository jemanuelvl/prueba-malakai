﻿using System;

namespace Models.Models
{
    /// <summary>
    /// Articles News
    /// </summary>
    public class Article
    {
        public Source source { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string urlToImage { get; set; }
        public DateTime publishedAt { get; set; }
        public string content { get; set; }

        public override bool Equals(Object obj)
        {
            if (obj is Article)
            {
                var that = obj as Article;
                return this.source == that.source && 
                    this.author == that.author && 
                    this.title == that.title &&
                    this.author == that.author &&
                    this.description == that.description &&
                    this.url == that.url &&
                    this.urlToImage == that.urlToImage &&
                    this.publishedAt == that.publishedAt &&
                    this.content == that.content;
            }

            return false;
        }
    }
}
