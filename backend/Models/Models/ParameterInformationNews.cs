﻿using Common.Attributes;

namespace Models.Models
{
    /// <summary>
    /// Model for getting all parameters information
    /// </summary>
    public class ParameterInformationNews
    {
        [Write(false)]
        public int IdParameter { get; set; }
        public string Parameter { get; set; }
        public int IdType { get; set; }
        public string Type { get; set; }
    }
}
