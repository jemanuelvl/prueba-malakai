using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.DTO;
using Services.Interfaces;
using WebAPI;
using System.Threading.Tasks;
using ServiceTest.Repository;

namespace Services.ServiceTest
{
    [TestClass]
    public class NewsServiceTest
    {
        private DependencyResolverHelper _serviceProvider;

        public NewsServiceTest()
        {

            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
        }


        [TestMethod]
        public async Task Getting_Param_InformationNews()
        {
            var newsService = _serviceProvider.GetService<INewsService>();
            var result = await newsService.GetAllParameters();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Getting_News_Null()
        {
            var newsService = _serviceProvider.GetService<INewsService>();
            QueryParamNews cosa  = new QueryParamNews();
            var result = await newsService.GetNews(cosa);
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public async Task Getting_News_NOT_Null()
        {
            var newsService = _serviceProvider.GetService<INewsService>();
            var result = await newsService.GetNews(MockedModels.queryParamNews);
            Assert.IsNotNull(result);
        }
    }
}
