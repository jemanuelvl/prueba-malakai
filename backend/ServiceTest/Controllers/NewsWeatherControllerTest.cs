﻿using AutoMapper;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.DTO;
using Models.Models;
using Services.Interfaces;
using ServiceTest.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI;
using WebAPI.Controllers;

namespace ServiceTest.Controllers
{
    [TestClass]
    public class NewsWeatherControllerTest
    {
        private DependencyResolverHelper _serviceProvider;
        private NewsWeatherController _newsWeatherController;

        public NewsWeatherControllerTest()
        {

            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
            _newsWeatherController = new NewsWeatherController(
                _serviceProvider.GetService<ILogger<NewsWeatherController>>(), _serviceProvider.GetService<INewsService>(), 
                _serviceProvider.GetService<IMapper>(), _serviceProvider.GetService<IWeatherService>(), 
                _serviceProvider.GetService<IHistoryService>());
        }

        [TestMethod]
        public async Task Get_Object_From_200OK_GettingParamNews()
        {
            var okResult = await _newsWeatherController.GettingParamNews();
            Assert.IsInstanceOfType(okResult, typeof(OkObjectResult));
            Assert.IsInstanceOfType((okResult as OkObjectResult).Value, typeof(GettingParamInformationNews));
        }

        [TestMethod]
        public async Task Get_Object_From_200OK_GetNewsAndWeather()
        {
            var okResult = await _newsWeatherController.GetNewsAndWeather(MockedModels.queryParamNewsWeather);
            Assert.IsInstanceOfType(okResult, typeof(OkObjectResult));
            Assert.IsInstanceOfType((okResult as OkObjectResult).Value, typeof(NewsWatherResponse));
        }

        [TestMethod]
        public async Task Get_Object_From_200OK_GetHistory()
        {
            var okResult = await _newsWeatherController.GetHistory();
            Assert.IsInstanceOfType(okResult, typeof(OkObjectResult));
            Assert.IsInstanceOfType((okResult as OkObjectResult).Value, typeof(IEnumerable<History>));
        }
    }
}
