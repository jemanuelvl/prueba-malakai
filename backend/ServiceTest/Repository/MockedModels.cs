﻿using Models.DTO;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceTest.Repository
{
    public static class MockedModels
    {
        public static QueryParamNews queryParamNews = new QueryParamNews()
        {
            q = "London",
            language = "en",
        };
        public static QueryParamNewsWeather queryParamNewsWeather = new QueryParamNewsWeather()
        {
            q = "London",
            language = "en",
        };
        public static IEnumerable<History> histories = new List<History>()
        {
            new History{
                City = "Lima",
                NewsFounded ="https://www.antena2.com/futbol/liga-betplay/tolima-explico-porque-no-renovo-a-montero-y-critico-el-anuncio-de-millonarios;",
                DateCreated = DateTime.Parse("2021-10-28 01:15:33.920"),
                WeatherResult = "16.090000000000032°C Resumen: muy nuboso",
                NewsFiltered = true
            },
            new History{
                City = "Lima",
                NewsFounded = String.Empty,
                DateCreated = DateTime.Parse("2021-10-28 01:17:24.413"),
                WeatherResult = "16.090000000000032°C Resumen: muy nuboso",
                NewsFiltered = true
            }
        };
        public static History history= new History{
            City = "Test",
            NewsFounded ="https://www.test.com",
            WeatherResult = "16.090000000000032°C Resumen: muy nuboso",
            NewsFiltered = true
        };
        public static History historyFailed = new History { 

            NewsFounded = "https://www.test.com",
            WeatherResult = "16.090000000000032°C Resumen: muy nuboso",
            NewsFiltered = true
        };
    }

}
