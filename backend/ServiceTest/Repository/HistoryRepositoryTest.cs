﻿using Common.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Models;
using Moq;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceTest.Repository
{
    [TestClass]
    public class HistoryRepositoryTest
    {
        [TestMethod]
        public async Task Getting_History()
        {
            var repository = new Mock<IHistoryRepository>();
            var historyRepo = repository.Object;
            var result = await historyRepo.GetHistoryRequest();
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(IEnumerable<History>));
        }
        [ExpectedException(typeof(GlobalExceptionError))]
        [TestMethod]
        public async Task Insert_History_Failed()
        {
            var repository = new Mock<IHistoryRepository>();
            repository.CallBase = true;
            repository.Setup(x => x.InsertHistoryRequest(MockedModels.historyFailed)).
                ThrowsAsync( new GlobalExceptionError(""));
        }
    }
}
