﻿using Repository.Interfaces;

namespace UnitOfWork.Interfaces
{
    public interface IUnitOfWorkRepository
    {
        INewsRepository NewsRepository { get; }
        IHistoryRepository HistoryRepository { get; }
    }
}
