﻿using Services.Interfaces;
using System.Collections;
using System.Threading.Tasks;
using UnitOfWork.Interfaces;

namespace ServiceGrpcTest.Services
{
    public class GRPCTestService : IGRPCTestService
    {
        private IUnitOfWork _unitOfWork;
        public GRPCTestService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable> ObtenerTests()
        {
            using (var unit = this._unitOfWork.CreateRepositoryGRPC_Test())
            {
                return await unit.Repositories.TestRepository.GetAll();
            }
        }
    }
}
