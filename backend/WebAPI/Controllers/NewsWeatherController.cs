﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models.DTO;
using Services.Interfaces;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NewsWeatherController : ControllerBase
    {

        private readonly ILogger<NewsWeatherController> _logger;
        private readonly INewsService _newsService;
        private readonly IWeatherService _weatherService;
        private readonly IHistoryService _historyService;
        private readonly IMapper _mapper;

        public NewsWeatherController(ILogger<NewsWeatherController> logger, 
            INewsService newsService, 
            IMapper mapper, 
            IWeatherService weatherService,
            IHistoryService historyService)
        {
            _logger = logger;
            _newsService = newsService;
            _mapper = mapper;
            _weatherService = weatherService;
            _historyService = historyService;
        }
        /// <summary>
        /// Getting All parameters for calling News API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GettingParamNews()
        {
            _logger.LogTrace($"Starting Controller {nameof(GettingParamNews)}");
            var result =  await _newsService.GetAllParameters();
            _logger.LogTrace($"End Controller {nameof(GettingParamNews)}");
            return Ok(result);
        }

        /// <summary>
        /// Getting All parameters for calling News API
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetNewsAndWeather")]
        public async Task<IActionResult> GetNewsAndWeather([FromBody]QueryParamNewsWeather query)
        {
            _logger.LogTrace($"Starting Controller {nameof(GettingParamNews)}");
            NewsWatherResponse newsWatherResponse = new NewsWatherResponse();
            newsWatherResponse.News = await _newsService.GetNews(_mapper.Map<QueryParamNews>(query));
            newsWatherResponse.Weathers = await _weatherService.GetWeathers(_mapper.Map<QueryParamWeather>(query));
            await _historyService.InsertHistoryRequest(newsWatherResponse.News, newsWatherResponse.Weathers, query);
            _logger.LogTrace($"End Controller {nameof(GettingParamNews)}");
            return Ok(newsWatherResponse);
        }

        /// <summary>
        /// Get History
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetHistory")]
        public async Task<IActionResult> GetHistory()
        {
            _logger.LogTrace($"Starting Controller {nameof(GettingParamNews)}");
            var result = await _historyService.GetHistory();
            _logger.LogTrace($"End Controller {nameof(GettingParamNews)}");
            return Ok(result);
        }
    }
}
