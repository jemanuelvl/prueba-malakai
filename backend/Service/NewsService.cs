﻿using Models.DTO;
using Services.Interfaces;
using System.Threading.Tasks;
using UnitOfWork.Interfaces;
using System.Linq;
using Microsoft.Extensions.Logging;
using Common.Constants;
using Models.Models;
using Microsoft.Extensions.Configuration;
using System;

namespace Service
{
    public class NewsService : INewsService
    {
        private readonly IUnitOfWork _unit;
        private readonly IConfiguration _configuration;
        private readonly ILogger<NewsService> _logger;
        public NewsService(IUnitOfWork unit, ILogger<NewsService> logger, IConfiguration configuration)
        {
            _unit = unit;
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Get All parameters information of NewsApi
        /// </summary>
        /// <returns></returns>
        public async Task<GettingParamInformationNews> GetAllParameters()
        {
            _logger.LogTrace($"INTO Service {nameof(GetAllParameters)}");
            using (var uwork = this._unit.CreatePruebaMalakaiDB())
            {
                var result = await uwork.Repositories.NewsRepository.GetAllParameters();
                GettingParamInformationNews information = new GettingParamInformationNews();
                var properties = information.GetType().GetProperties();
                foreach(var prop in properties)
                {
                    prop.SetValue(information , result.Where(x => x.Type == prop.Name).Select(x=>x.Parameter).ToList());
                }
                _logger.LogTrace($"END Service {nameof(GetAllParameters)}");
                return information;
            }
        }

        public async Task<NewsResponse> GetNews(QueryParamNews parameters)
        {
            _logger.LogTrace($"INTO Service {nameof(GetNews)}");
            var result = new NewsResponse();
            if (!String.IsNullOrEmpty(parameters.q))
                result = await Common.Utils.Request.GetCallAPI<NewsResponse>(
                    _configuration[GlobalConstants.NEWS_URL_API] + GlobalConstants.TOP_NEWS,
                    _configuration[GlobalConstants.NEWS_API_KEY],
                    parameters);
            else
                return null;

            _logger.LogTrace($"END Service {nameof(GetNews)}");
            return result;
        }
    }
}
