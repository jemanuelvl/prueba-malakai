﻿using Common.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.DTO;
using Models.Models;
using Services.Interfaces;
using System.Threading.Tasks;
using UnitOfWork.Interfaces;

namespace Service
{
    public class WeatherService : IWeatherService
    {
        private readonly IUnitOfWork _unit;
        private readonly IConfiguration _configuration;
        private readonly ILogger<WeatherService> _logger;
        public WeatherService(IUnitOfWork unit, ILogger<WeatherService> logger, IConfiguration configuration)
        {
            _unit = unit;
            _logger = logger;
            _configuration = configuration;
        }
        public async Task<WeatherResponse> GetWeathers(QueryParamWeather queryParam)
        {
            _logger.LogTrace($"INTO Service {nameof(GetWeathers)}");
            var result = await Common.Utils.Request.GetCallAPI<WeatherResponse>(
                _configuration[GlobalConstants.WEATHER_URL_API] + GlobalConstants.WEATHER,
                _configuration[GlobalConstants.WEATHER_API_KEY],
                queryParam, true);
                _logger.LogTrace($"END Service {nameof(GetWeathers)}");
                return result;
            
        }
    }
}
