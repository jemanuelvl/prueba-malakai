﻿using AutoMapper.Configuration;
using Microsoft.Extensions.Logging;
using Models.Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using UnitOfWork.Interfaces;
using Models.DTO;

namespace Service
{
    public class HistoryService : IHistoryService
    {
        private readonly IUnitOfWork _unit;
        private readonly ILogger<HistoryService> _logger;
        public HistoryService(IUnitOfWork unit, ILogger<HistoryService> logger)
        {
            _unit = unit;
            _logger = logger;
        }
        public async Task<IEnumerable<History>> GetHistory()
        {
            _logger.LogTrace($"INTO Service {nameof(GetHistory)}");
            using (var uwork = this._unit.CreatePruebaMalakaiDB())
            {
                var result = await uwork.Repositories.HistoryRepository.GetHistoryRequest();
                _logger.LogTrace($"END Service {nameof(GetHistory)}");
                return result;
            }
        }

        public async Task InsertHistoryRequest(NewsResponse news, WeatherResponse weathers, QueryParamNewsWeather param)
        {
            _logger.LogTrace($"INTO Service {nameof(InsertHistoryRequest)}");
            using (var uwork = this._unit.CreatePruebaMalakaiDB())
            {
                History history = new History()
                {
                    City = param.q,
                    NewsFounded = GetUrlText(news.articles),
                    WeatherResult = $"{this.GetKevinToCelcius(weathers.Main!=null? weathers.Main.Temp: 273.15)} Resumen: {GetWatherResponse(weathers.Weather)}",
                    NewsFiltered = CheckNewsFiltered(param)
                };
                await uwork.Repositories.HistoryRepository.InsertHistoryRequest(history);
            }
            _logger.LogTrace($"END Service {nameof(InsertHistoryRequest)}");
        }

        private bool CheckNewsFiltered(QueryParamNewsWeather param)
        {
            if (param.language!="es" || !String.IsNullOrEmpty(param.country) || !String.IsNullOrEmpty(param.category))
            {
                return true;
            }
            return false;
        }

        private string GetKevinToCelcius(double temp)
        {
            return (temp - 273.15).ToString()+ "°C";
        }

        private string GetWatherResponse(Weather[] weathers)
        {
            return weathers.Any() ? weathers.FirstOrDefault().Description : "No Description";
        }

        private string GetUrlText(IEnumerable<Article> articles)
        {
            if (articles != null)
            {
                StringBuilder strb = new StringBuilder();
                for (int i=0; i< articles.Count(); i++)
                {
                    if (i > 9)
                        break;
                    else
                        strb.Append(articles.ElementAt(i).url + ";");
                }
                    
                return strb.ToString();
            }
            return String.Empty;
        }
   
    }
}
