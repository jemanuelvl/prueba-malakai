﻿using Models.DTO;
using Models.Models;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IWeatherService
    {
        Task<WeatherResponse> GetWeathers(QueryParamWeather queryParam);
    }
}
