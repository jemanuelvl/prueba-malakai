﻿using Models.DTO;
using Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IHistoryService
    {
        Task<IEnumerable<History>> GetHistory();
        Task InsertHistoryRequest(NewsResponse news, WeatherResponse weathers, QueryParamNewsWeather city);
    }
}
