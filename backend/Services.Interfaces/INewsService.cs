﻿using Models.DTO;
using Models.Models;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface INewsService
    {
        Task<GettingParamInformationNews> GetAllParameters();
        Task<NewsResponse> GetNews(QueryParamNews parameters);
    }
}
