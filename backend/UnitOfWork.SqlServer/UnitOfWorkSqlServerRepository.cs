﻿using Repository.Interfaces;
using Repository.SqlServer;
using System.Data.SqlClient;
using UnitOfWork.Interfaces;

namespace UnitOfWork.SqlServer
{
    public class UnitOfWorkSqlServerRepository : IUnitOfWorkRepository
    {
        public INewsRepository NewsRepository { get; }
        public IHistoryRepository HistoryRepository { get; }
        public UnitOfWorkSqlServerRepository(SqlConnection context, SqlTransaction transaction)
        {
            this.NewsRepository = new NewsRepository(context,
                transaction);
            this.HistoryRepository = new HistoryRepository(context,
                transaction);
        }
        
    }
}
