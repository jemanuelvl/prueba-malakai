﻿using Common.Constants;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using UnitOfWork.Interfaces;

namespace UnitOfWork.SqlServer
{
    public class UnitOfWorkSqlServer : IUnitOfWork
    {
        private readonly IConfiguration _configuration;

        public UnitOfWorkSqlServer(IConfiguration configuration = null)
        {
            _configuration = configuration;
        }

        public IUnitOfWorkAdapter CreatePruebaMalakaiDB()
        {
            var connectionString =
                new SqlConnection(_configuration.GetConnectionString(GlobalConstants.CONNECT_DBPRUEBA_MK));

            return new UnitOfWorkSqlServerAdapter(connectionString);
        }
    }
}
