﻿using Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IHistoryRepository
    {
        Task<IEnumerable<History>> GetHistoryRequest();
        Task InsertHistoryRequest(History history);
    }
}
