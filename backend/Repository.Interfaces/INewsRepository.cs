﻿using Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    /// <summary>
    /// Contract for Test repository
    /// </summary>
    public interface INewsRepository
    {
        Task<IEnumerable<ParameterInformationNews>> GetAllParameters();
    }
}
