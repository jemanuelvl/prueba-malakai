﻿using Common.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Utils
{
    public class Request
    {
        public static async Task<object> PostCallAPI(string url, object jsonObject, string apikey)
        {
            using (HttpClient client = new HttpClient())
            {
                var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
                client.DefaultRequestHeaders.Add("apiKey", apikey);
                var response = await client.PostAsync(url, content);
                if (response != null)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<object>(jsonString);
                }
                return null;
            }
        }
        public static async Task<T> GetCallAPI<T>(string url, string apikey) where T: class
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apikey);
                var response = await client.GetAsync(url);
                if (response != null)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(jsonString);
                }
                return null;
            }
        }
        public static async Task<T> GetCallAPI<T>(string url, string apikey, Dictionary<string, string> parameters) where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("apiKey", apikey);
                var uriBuilder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                foreach(var parame in parameters)
                {
                    query[parame.Key] = parame.Value;
                }
                uriBuilder.Query = query.ToString();
                url = uriBuilder.ToString();
                var response = await client.GetAsync(url);
                if (response != null)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(jsonString);
                }
                return null;
            }
        }
        public static async Task<T> GetCallAPI<T>(string url, string apikey, object parameters) where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(GlobalConstants.AUTHORIZATION, apikey);
                var uriBuilder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                var properties = parameters.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    if(prop.GetValue(parameters)!=null)
                        query[prop.Name] = prop.GetValue(parameters).ToString();
                }
                uriBuilder.Query = query.ToString();
                url = uriBuilder.Uri.AbsoluteUri.ToString();
                var response = await client.GetAsync(url);
                if (response != null)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(jsonString);
                }
                return null;
            }
        }
        public static async Task<T> GetCallAPI<T>(string url, string apikey, object parameters, bool apiKeyInHeader = false) where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                if(!apiKeyInHeader)
                    client.DefaultRequestHeaders.Add(GlobalConstants.AUTHORIZATION, apikey);

                var uriBuilder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                var properties = parameters.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    if (prop.GetValue(parameters) != null)
                        query[prop.Name] = prop.GetValue(parameters).ToString();
                }
                query[GlobalConstants.APP_ID] = apikey;
                uriBuilder.Query = query.ToString();
                url = uriBuilder.Uri.AbsoluteUri.ToString();
                var response = await client.GetAsync(url);
                if (response != null)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(jsonString);
                }
                return null;
            }
        }
    }
}
