﻿
namespace Common.Constants
{
    public class GlobalConstants
    {
        public static readonly string NEWS_API_KEY = "Urls:NewsAPIKey";
        public static readonly string NEWS_URL_API = "Urls:NewsUrl";
        public static readonly string FRONT_URL = "Urls:frontendUrl";
        public static readonly string WEATHER_API_KEY = "Urls:WeatherAPIKey";
        public static readonly string WEATHER_URL_API = "Urls:WeatherUrl";
        public static readonly string CONNECT_DBPRUEBA_MK = "MalakaiPrueba";
        public static readonly string AUTHORIZATION = "Authorization";
        public static readonly string APP_ID = "appid";
        public static readonly string EVERYTHING = "everything?";
        public static readonly string TOP_NEWS = "top-headlines?";
        public static readonly string WEATHER = "weather?";
    }
}
