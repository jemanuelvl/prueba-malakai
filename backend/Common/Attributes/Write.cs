﻿using System;

namespace Common.Attributes
{
    /// <summary>
    /// Attribute for writting properties in mapping ExecuteSql
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class Write : System.Attribute
    {
        //True when is not for read
        public bool value;
        public Write(bool value)
        {
            this.value = value;
        }
    }
}
