﻿using System;

namespace Common.Attributes
{
    /// <summary>
    /// Attribute for reading properties in mapping GettingData from Sql
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class Read : System.Attribute
    {
        //False when is not for read
        public bool value;
        public Read(bool value)
        {
            this.value = value;
        }
    }
}
