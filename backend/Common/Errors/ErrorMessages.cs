﻿namespace Common.Errors
{
    public class ErrorMessages
    {
        public static readonly string ERROR_ON_MAPPING_OBJECT_RESULT = "ERROR_ON_MAPPING_OBJECT_RESULT";
        public static readonly string ERROR_ON_EXCECUTE_STORE_PROCEDURE = "ERROR_ON_EXCECUTE_STORE_PROCEDURE";
        public static readonly string NEWS_NOT_FOUNDED = "NEWS_NOT_FOUNDED";
    }
}
